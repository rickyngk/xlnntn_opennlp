/*
Use model from http://opennlp.sourceforge.net/models-1.5/
Docs & tutor:
	http://www.programcreek.com/2012/05/opennlp-tutorial/
	http://opennlp.apache.org/documentation/1.5.3/manual/opennlp.html#opennlp
	http://danielmclaren.com/2007/05/11/getting-started-with-opennlp-natural-language-processing
Resource:
	http://disi.unitn.it/moschitti/corpora.htm
*/
package com.tttc.xlnntn;
import java.io.*;
import opennlp.tools.util.*;
import opennlp.tools.sentdetect.*;
import opennlp.tools.tokenize.*;
import opennlp.tools.namefind.*;
import opennlp.tools.postag.*;
import opennlp.tools.chunker.*;
import opennlp.tools.parser.*;
import opennlp.tools.cmdline.parser.ParserTool;
import java.nio.charset.Charset;
import opennlp.tools.doccat.*;
import java.util.*;

public class Helper {
	private static String GetPath() {
		return (new File(Helper.class.getProtectionDomain().getCodeSource().getLocation().getPath())).getParentFile().getPath();
	}

	public static List<String> SentenceDetect(String paragraph) throws InvalidFormatException, IOException {	 
		InputStream is = Helper.class.getClass().getResourceAsStream("/assets/en-sent.bin");
		SentenceModel model = new SentenceModel(is);
		is.close();

		SentenceDetectorME sdetector = new SentenceDetectorME(model);
		String sentences[] = sdetector.sentDetect(paragraph);
		
		List<String> list = new ArrayList<String>(Arrays.asList(sentences));
		return list;
	}

	public static List<String> Tokenize(String input) throws InvalidFormatException, IOException {
		InputStream is = Helper.class.getClass().getResourceAsStream("/assets/en-token.bin");
		TokenizerModel model = new TokenizerModel(is);
		is.close();

		Tokenizer tokenizer = new TokenizerME(model);
		String tokens[] = tokenizer.tokenize(input);

		List<String> list = new ArrayList<String>(Arrays.asList(tokens));
		return list;
	}

	public static List<Span> FindName(List<String> input) throws IOException {
		InputStream is = Helper.class.getClass().getResourceAsStream("/assets/en-ner-person.bin");
		TokenNameFinderModel model = new TokenNameFinderModel(is);
		is.close();

		NameFinderME names = new NameFinderME(model);
		Span nameSpans[] = names.find(input.toArray(new String[input.size()]));

		List<Span> list = new ArrayList<Span>(Arrays.asList(nameSpans));
		return list;
	}

	public static String[] PosGetTags(String input) throws IOException {
		InputStream is = Helper.class.getClass().getResourceAsStream("/assets/en-pos-maxent.bin");
		POSModel model = new POSModel(is);
		is.close();

		POSTaggerME tagger = new POSTaggerME(model);

		String whitespaceTokenizerLine[] = WhitespaceTokenizer.INSTANCE.tokenize(input);
		String[] tags = tagger.tag(whitespaceTokenizerLine);
		return tags;
	}

	public static POSSample PosTag(String input) throws IOException { 
		return new POSSample(WhitespaceTokenizer.INSTANCE.tokenize(input), PosGetTags(input));
	}

	public static List<Span> Chunk(String input) throws IOException {
		InputStream is = Helper.class.getClass().getResourceAsStream("/assets/en-chunker.bin");
		ChunkerModel cModel = new ChunkerModel(is);
		is.close();

		ChunkerME chunkerME = new ChunkerME(cModel);

		String whitespaceTokenizerLine[] = WhitespaceTokenizer.INSTANCE.tokenize(input);
		String[] tags = PosGetTags(input);
		Span[] span = chunkerME.chunkAsSpans(whitespaceTokenizerLine, tags);

		List<Span> list = new ArrayList<Span>(Arrays.asList(span));
		return list; 
	}

	public static List<Parse> Parse(String input) throws InvalidFormatException, IOException {
		InputStream is = Helper.class.getClass().getResourceAsStream("/assets/en-parser-chunking.bin");
		ParserModel model = new ParserModel(is);
		is.close();

		Parser parser = ParserFactory.create(model);
		Parse topParses[] = ParserTool.parseLine(input, parser, 1);

		List<Parse> list = new ArrayList<Parse>(Arrays.asList(topParses));
		return list; 
	}

	public static void DocCategoryTraining(String input, String output) throws InvalidFormatException, IOException {
		DoccatModel model = null;

		InputStream dataIn = null;
		try {
			dataIn = new FileInputStream(GetPath() + "/" + input);
			ObjectStream<String> lineStream =
				new PlainTextByLineStream(dataIn, "UTF-8");
			ObjectStream<DocumentSample> sampleStream = new DocumentSampleStream(lineStream);

			model = DocumentCategorizerME.train("en", sampleStream);

			OutputStream modelOut = null;
			try {
				modelOut = new BufferedOutputStream(new FileOutputStream(GetPath() + "/" + output));
				model.serialize(modelOut);
			}
			catch (IOException e) {
				// Failed to save model
				e.printStackTrace();
			}
			finally {
				if (modelOut != null) {
					try {
						 modelOut.close();
					}
					catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (dataIn != null) {
				try {
					dataIn.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String DocCategory(String inputText, String training_data) throws InvalidFormatException, IOException {
		InputStream is = null;
		is = Helper.class.getClass().getResourceAsStream("/assets/" + training_data);
		if (is == null) {
			is = new FileInputStream(GetPath() + "/" + training_data);
		}
			
		DoccatModel m = new DoccatModel(is);
		DocumentCategorizerME myCategorizer = new DocumentCategorizerME(m);
		double[] outcomes = myCategorizer.categorize(inputText);
		String category = myCategorizer.getBestCategory(outcomes);
		return category;
	}

	public static String DocCategory(String inputText) throws InvalidFormatException, IOException {
		String training_data = "en-setiment-samples.bin";
		InputStream is = null;
		is = Helper.class.getClass().getResourceAsStream("/assets/" + training_data);
		if (is == null) {
			is = new FileInputStream(GetPath() + "/" + training_data);
		}
			
		DoccatModel m = new DoccatModel(is);
		DocumentCategorizerME myCategorizer = new DocumentCategorizerME(m);
		double[] outcomes = myCategorizer.categorize(inputText);
		String category = myCategorizer.getBestCategory(outcomes);
		return category;
	}
}




