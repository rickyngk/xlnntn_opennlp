
import com.tttc.xlnntn.*;
import java.util.List;

public class Demo {
	static void cls() {
		
		if( System.getProperty( "os.name" ).startsWith( "Window" ) ) {
			try {
				Runtime.getRuntime().exec("cls");
			}
			catch (Exception E){}
		} else {
			final String ANSI_CLS = "\u001b[2J";
			final String ANSI_HOME = "\u001b[H";
			System.out.print(ANSI_CLS + ANSI_HOME);
			System.out.flush();
		}
	}

	static void _wait() {
		System.out.print("Press enter to continue ...");
		System.console().readLine();
	}

	static int Menu(String title, String[]options) {
		cls();
		System.out.println(title);
		System.out.println("------------------------------------------------------");
		for (int i = 1; i < options.length; i++) {
			System.out.println("[" + i + "] " + options[i]);
		}
		System.out.println("");
		System.out.println("[0] " + options[0]);
		System.out.println("------------------------------------------------------");
		while (true) {
			System.out.print("Enter your request: " );
			String input = System.console().readLine();
			int opt = -1;
			try {
				opt = Integer.parseInt(input);
			}
			catch (Exception e) {
				opt = -1;
			}
			if (opt >= 0 && opt < options.length) {
				return opt;
			}
		}
	}
	public static void main(String[] args) {
		String defaultText = "Armacost said the U.S. Hopes Japan will take steps to lift its domestic economy and reduce dependence on exports, remove barriers to imports and settle outstanding trade issues.";
		boolean loop = true;
		while (loop) {
			try {
			
				int opt = Menu("Main menu", new String[]{
					"Exit",
					"Demo: Detect sentences",
					"Demo: Tokenize",
					"Demo: Find name",
					"Demo: POS Tag",
					"Demo: Chunk",
					"Demo: Parser",
					"Demo: Document category training & detecting",
					"Demo: Document category (use default trained data)"
				});
				if (opt == 0) {
					loop = false;
					System.out.println("THANK YOU!" );
				}
				else if (opt == 1) { //Detect sentences
					opt = Menu("Detect sentences", new String[]{
						"Back",
						"Enter text",
						"Use sample text"
					});
					String text = "";
					if (opt != 0) {
						if (opt == 1) {
							System.out.print("Enter text: " );
							text = System.console().readLine();
						}
						else {
							text = defaultText;
							System.out.println("Default text: " + text);
						}
						System.out.println("--- RESULT ---");
						List<String> list = Helper.SentenceDetect(text);
						for (String i:list) {
							System.out.println(i);
						}
						System.out.println("--------------");
						_wait();
					}
				}
				else if (opt == 2) { //Tokenize
					opt = Menu("Tokenize", new String[]{
						"Back",
						"Enter text",
						"Use sample text"
					});
					String text = "";
					if (opt != 0) {
						if (opt == 1) {
							System.out.print("Enter text: " );
							text = System.console().readLine();
						}
						else {
							text = defaultText;
							System.out.println("Default text: " + text);
						}
						System.out.println("--- RESULT ---");
						List<String> list = Helper.Tokenize(text);
						for (int i = 0; i < list.size(); i++) {
							System.out.println(i + ":" + list.get(i));
						}
						System.out.println("--------------");
						_wait();
					}
				}
				else if (opt == 3) { //Find name
					opt = Menu("Find name", new String[]{
						"Back",
						"Enter text",
						"Use sample text"
					});
					String text = "";
					if (opt != 0) {
						if (opt == 1) {
							System.out.print("Enter text: " );
							text = System.console().readLine();
						}
						else {
							text = defaultText;
							System.out.println("Default text: " + text);
						}
						System.out.println("--- RESULT ---");
						System.out.println("* TOKEN:");
						List<String> list = Helper.Tokenize(text);
						for (int i = 0; i < list.size(); i++) {
							System.out.println(i + ":" + list.get(i));
						}
						System.out.println("* Name span:");
						List<opennlp.tools.util.Span> list2 = Helper.FindName(list);
						for (opennlp.tools.util.Span i:list2) {
							System.out.println(i);
						}
						System.out.println("--------------");
						_wait();
					}
				}
				else if (opt == 4) { //POS tag
					opt = Menu("POS tag", new String[]{
						"Back",
						"Enter text",
						"Use sample text"
					});
					String text = "";
					if (opt != 0) {
						if (opt == 1) {
							System.out.print("Enter text: " );
							text = System.console().readLine();
						}
						else {
							text = defaultText;
							System.out.println("Default text: " + text);
						}
						System.out.println("--- RESULT ---");
						opennlp.tools.postag.POSSample sample =  Helper.PosTag(text);
						System.out.println(sample);
						System.out.println("--------------");
						_wait();
					}
				}
				else if (opt == 5) { //Chunk
					opt = Menu("Chunk", new String[]{
						"Back",
						"Enter text",
						"Use sample text"
					});
					String text = "";
					if (opt != 0) {
						if (opt == 1) {
							System.out.print("Enter text: " );
							text = System.console().readLine();
						}
						else {
							text = defaultText;
							System.out.println("Default text: " + text);
						}
						System.out.println("--- RESULT ---");
						System.out.println("* TOKEN:");
						List<String> list = Helper.Tokenize(text);
						for (int i = 0; i < list.size(); i++) {
							System.out.println(i + ":" + list.get(i));
						}
						System.out.println("* CHUNK:");
						List<opennlp.tools.util.Span> list2 =  Helper.Chunk(text);
						for (opennlp.tools.util.Span i:list2) {
							System.out.println(i);
						}
						System.out.println("--------------");
						_wait();
					}
				}
				else if (opt == 6) { //Parser
					opt = Menu("Parser", new String[]{
						"Back",
						"Enter text",
						"Use sample text"
					});
					String text = "";
					if (opt != 0) {
						if (opt == 1) {
							System.out.print("Enter text: " );
							text = System.console().readLine();
						}
						else {
							text = defaultText;
							System.out.println("Default text: " + text);
						}
						System.out.println("--- RESULT ---");
						List<opennlp.tools.parser.Parse> parses =  Helper.Parse(text);
						for (opennlp.tools.parser.Parse parse:parses) {
							parse.show();
						}
						System.out.println("--------------");
						_wait();
					}
				}
				else if (opt == 7) { //doc cat
					opt = Menu("Document category training & detection", new String[]{
						"Back",
						"Enter text",
						"Use sample text"
					});
					String text = "";
					if (opt != 0) {
						if (opt == 1) {
							System.out.print("Enter text: " );
							text = System.console().readLine();
						}
						else {
							text = defaultText;
							System.out.println("Default text: " + text);
						}
						System.out.print("Enter train data file (without .train): ");
						String train = System.console().readLine();

						System.out.println("--- RESULT ---");
						System.out.println("*** TRAINING ***");
						Helper.DocCategoryTraining(train + ".train", train + ".bin");
						System.out.println("*** CATEGORY ***");
						System.out.println(Helper.DocCategory(text, train + ".bin"));
						System.out.println("--------------");
						_wait();
					}
				}
				else if (opt == 8) { //doc cat
					opt = Menu("Document category", new String[]{
						"Back",
						"Enter text",
						"Use sample text"
					});
					String text = "";
					if (opt != 0) {
						if (opt == 1) {
							System.out.print("Enter text: " );
							text = System.console().readLine();
						}
						else {
							text = defaultText;
							System.out.println("Default text: " + text);
						}
						System.out.println("--- RESULT ---");
						System.out.println(Helper.DocCategory(text));
						System.out.println("--------------");
						_wait();
					}
				}
			}
			catch (Exception E) {
				E.printStackTrace();
				_wait();
			}
		}
	}
}