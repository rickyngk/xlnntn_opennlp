from __future__ import with_statement
import sys, os, shutil, os.path, distutils, distutils.dir_util, subprocess, fnmatch, zipfile, ctypes
from config import *
gPathStack = []
gHasError = False
#----------------------------------------------------------
# System function
#----------------------------------------------------------
STD_OUTPUT_HANDLE = -11
gTextColor = {'black': 0, 'blue': 1, 'green': 2, 'aqua': 3, 'red': 4, 'purple': 5, 'yellow': 6, 'white': 7, 'grey': 8, 'light blue': 9, 'light green': 10, 'light aqua': 11, 'light red': 12, 'light purple': 13, 'light yellow': 14, 'bright white': 15}
gTextColorMAC = {'black': "30;49m", 'red': "31;49m", 'green': "32;49m", 'yellow': "33;49m", 'blue': "34;49m", 'purple': "35;49m", 'aqua': "36;49m", 'white': "37;49m", 'grey': "30;49m", 'light red': "31;49m", 'light green': "32;49m", 'light yellow': "33;49m", 'light blue': "34;49m", 'light purple': "35;49m", 'light aqua': "36;49m", 'bright white': "37;49m"}
def get_csbi_attributes(handle):
	# Based on IPython's winconsole.py, written by Alexander Belchenko
	import struct
	csbi = ctypes.create_string_buffer(22)
	res = ctypes.windll.kernel32.GetConsoleScreenBufferInfo(handle, csbi)
	assert res

	(bufx, bufy, curx, cury, wattr,
	left, top, right, bottom, maxx, maxy) = struct.unpack("hhhhHhhhhhh", csbi.raw)
	return wattr

def echoc(text, color):
	if os.name=="nt": 
		if gTextColor.has_key(color):
			color = gTextColor[color]
			handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
			reset = get_csbi_attributes(handle)
			ctypes.windll.kernel32.SetConsoleTextAttribute(handle, color)
			print(text)
			ctypes.windll.kernel32.SetConsoleTextAttribute(handle, reset)
		else:
			print(text)
	else:
		if gTextColorMAC.has_key(color):
			color = gTextColorMAC[color]
			CSI="\x1B[1;"
			print CSI + color + text + CSI + "0m"
		else:
			print text

def echowarn(text):
	echoc(text, "light yellow")
	
def echoe(text):
	echoerror(text)
	
def echoerror(text):
	Error()
	echoc(text, "light red")

def echo(str):
	print str

def md(dir):
	ExistDir = exist(dir)
	notIsFile = os.path.isfile(dir)
	if (not ExistDir) or (ExistDir and notIsFile):
		try:
			os.makedirs(dir)
		except OSError, e:
			echoerror("[ERROR] [md] fail when execute with dir [%s]" %(dir))
			Error()

def rd(dir):
	if exist(dir):
		try:
			shutil.rmtree(dir)
		except:
			echoerror("[ERROR] [rd] fail when execute with dir [%s]" %(dir))
			Error()
	else:
		echowarn("[WARNING] [rd] %s not found" %(dir))

def exist(path):
	return os.path.exists(path)
	
	
def cd(path):
	if exist(path):
		os.chdir(path)
	else:
		echoerror("[ERROR] [cd] fail when execute with dir [%s]" %(path))
		Error()

def pushd(path):
	global gPathStack
	if exist(path):
		gPathStack.append(CurrentDir())
		cd(path)
	else:
		echoerror("[ERROR] [pushd] fail when execute with dir [%s]" %(path))
		Error()
		
def popd():
	global gPathStack
	empty = (len(gPathStack) == 0)
	if not empty:
		cd(gPathStack.pop())
	
def goto(lable):
	lable()
	
def xcopy(src, dst):
	try:
		distutils.dir_util.copy_tree(src, dst)
	except OSError as exc:
		echoerror("[ERROR] [xcopy] fail to execute with files [%s] [%s]" %(src, dst))
		Error()
		
def copy(src, dst):
	if exist(src):
		try:
			shutil.copy(src, dst)
		except ValueError:
			echoerror("[ERROR] [copy] fail to execute with files [%s] [%s]: [%s]" %(src, dst, ValueError))
	else:
		echoerror("[ERROR] [copy] fail. [%s] not found" %(src))
		Error()
		
def move(src, dst):
	if exist(src):
		try:
			shutil.move(src, dst)
		except:
			echoerror("[ERROR] [move] fail to execute with files [%s] [%s]" %(src, dst))
	else:
		echoerror("[ERROR] [move] fail. [%s] not found" %(src))
		Error()
		
def delete(file):
	if exist(file):
		try: 
			os.remove(file)
		except:
			echoerror("[ERROR] [delete] fail to execute with file [%s]" %(file))
			Error()
		
def pause(message):
	raw_input(message)	

def cls():
	if os.name=="nt":
		run("cls", True)
	else:
		run("clear", True)

#----------------------------------------------------------
# additional function
#----------------------------------------------------------
def run(cmd, IsShell = True):
	# return os.system(cmd)
	try:
		retcode = subprocess.call(cmd, shell=IsShell)
		return retcode
	except OSError, e:
		Error()
		echoerror("[ERROR] fail to execute run %s)" %(cmd))
		print(str(e))
		return -1

def Error():
	global gHasError
	gHasError = True

def CurrentDir():
	return os.getcwd()
	
def HasError():
	global gHasError
	return gHasError
	
def IsFileExist(path):
	return os.path.exists(path) and os.path.isfile(path)

def ListFileNoRecursive(path, filters):
	list = []
	params = filters.split(",")
	for filter in params:
		filter = filter.strip()
		for filename in os.listdir(path):
			if fnmatch.fnmatch(filename, filter):
				fn = os.path.basename(filename)
				if not fn.startswith("."):
					list.append(os.path.join(path, filename))
	return list
	
def ListFile(path, filters):
	list = []
	params = filters.split(",")
	for filter in params:
		filter = filter.strip()
		for root, dirnames, filenames in os.walk(path):
			for filename in fnmatch.filter(filenames, filter):
				fn = os.path.basename(filename)
				if not fn.startswith("."):
					list.append(os.path.join(root, filename))
	return list
	
def ListFileAll(path, filters):
	list = []
	params = filters.split(",")
	for filter in params:
		filter = filter.strip()
		for root, dirnames, filenames in os.walk(path):
			for filename in fnmatch.filter(filenames, filter):
				list.append(os.path.join(root, filename))	
	return list

def GetFileNX(long_file_path):
	return os.path.basename(long_file_path)


def GetFileN(long_file_path):
	filename = GetFileNX(long_file_path)
	return os.path.splitext(filename)[0]
	
def GetFileX(long_file_path):
	filename = GetFileNX(long_file_path)
	return os.path.splitext(filename)[1]
	
def GetFileLastModified(file):
	if exist(file):
		return time.ctime(os.path.getmtime(file))
	else:
		return 0

def GetFileSize(file):
	if exist(file):
		return os.path.getsize(file)
	else:
		return -1
			
def GetFileSizeAsText(file):
	if exist(file):
		s = os.path.getsize(file)
		if s < 1024:
			return str(s) + " bytes"
		elif s < 1024*1024.:
			return str(round(s/1024.0,2)) + " KBs"
		else:
			return str(round(s/(1024*1024.0), 2)) + " MBs"
	else:
		return "not exist"
		

def WriteFile(file, content):
	with open(file, 'a') as f:
		f.write(content)
		
def WriteNewFile(file, content):
	with open(file, 'w') as f:
		f.write(content)

def GetPath(file):
	if exist(file):
		return os.path.dirname(os.path.realpath(file))
	else:
		echoerror("[ERROR] [GetPath] file not found [%s]" %(file))
		Error()
		
def GetFileAbsolutePath(file):
	p = GetPath(file) + "/" + GetFileNX(file)
	p = p.replace("\\", "/")
	return p

def SetEnv(env, value):
	os.putenv(env, value)

def unzip(zipFilePath, destDir):
	zfile = zipfile.ZipFile(zipFilePath)
	for name in zfile.namelist():
		(dirName, fileName) = os.path.split(name)
		if fileName == '':
			# directory
			newDir = destDir + '/' + dirName
			if not os.path.exists(newDir):
				os.mkdir(newDir)
		else:
			# file
			fd = open(destDir + '/' + name, 'wb')
			fd.write(zfile.read(name))
			fd.close()
	zfile.close()

#----------------------------------------------------------
# main function
#----------------------------------------------------------
JAVA_BIN = ""

def makedata():
	echo("*** BUILD DATA ***")
	if exist("_build"):
		if exist("_build/data"):
			rd("_build/data")
	else:
		md("_build")
		
	md("_build/data")
	md("_build/data/###export")

	filelist = ListFile("./input", "*.train")
	train_text = []
	for filename in filelist:
		echo("+ Export: " + filename)
		echo(" 	- Unzipping...")

		train_text = []
		output = "_build/data/" + GetFileN(filename)
		md(output)
		unzip(filename, output)

		echo("	- Packing...")
		filelist2 = ListFile(output, "*")
		for fn in filelist2:
			path = fn.split("/")
			cat = path[len(path) - 2]

			f = open(fn, 'r')
			text = f.read()
			f.close()
			text = cat + " " + (" ".join(text.splitlines())).strip(' \t\n\r')
			train_text.append(text)
	
		WriteNewFile("_build/data/###export/" + GetFileN(filename) + ".train", "\n".join(train_text))


def debug():
	if exist("_build"):
		if exist("_build/classes"):
			rd("_build/classes")
		if exist("_build/jar"):
			rd("_build/jar")
		if exist("_build/export"):
			rd("_build/export")
	else:
		md("_build")
		makedata()

	if not exist("_build/data"): makedata()

	md("_build/classes")

	jarlist = ListFile("./libs", "*.jar")
	for i in range(len(jarlist)):
		jarlist[i] = '"' + GetFileAbsolutePath(jarlist[i]) + '"'
	if os.name=="nt": 
		jarlist = ";".join(jarlist)
	else:
		jarlist = ":".join(jarlist)

	filelist = ListFile("./source", "*.java")
	WriteNewFile("./_build/source.txt", "\n".join(filelist))

	cmd = '"%s"javac -g:none -d _build/classes -cp %s -encoding utf8 @./_build/source.txt'%(JAVA_BIN, jarlist)
	if run(cmd):
		Error()
		return

	mainClass = None
	filelist = ListFile("./_build/classes", "*.class")
	for filename in filelist:
		run('"%s"javap -c %s>%s'%(JAVA_BIN, filename.replace(".class", ""), filename + ".bytecode"))
		f = open(filename + ".bytecode", 'r')
		data = f.read()
		f.close()
		delete(filename + ".bytecode")
		if data.find("public static void main") >= 0:
			mainClass = GetFileAbsolutePath(filename).replace(CurrentDir().replace("\\", "/") + "/_build/classes/", "").replace("/", ".").replace(".class", "")
			break
	if mainClass == None:
		echoerror("No main class found")
		Error()
		return

	jarfile = mainClass.split(".")
	jarfile = jarfile[len(jarfile) - 1]

	md("_build/jar")

	md("_build/export")
	xcopy("_build/classes", "_build/export")
	xcopy("assets", "_build/export/assets")
	# xcopy("libs", "_build/export/libs")

	jarlist = ListFile("./libs", "*.jar")
	for i in range(len(jarlist)):
		jarlist[i] = "libs/" + GetFileNX(jarlist[i])
	jarlist = " ".join(jarlist)
	
	filelist = ListFileAll("_build/export", "*.*")
	for f in filelist:
		if GetFileN(f).startswith("."): 
			delete(f)

	WriteNewFile("_build/jar/manifest.mf", "Main-Class: %s\nClass-Path: %s\n"%(mainClass, jarlist))
	run('"%s"jar cvfm _build/jar/%s.jar _build/jar/manifest.mf -C _build/export .'%(JAVA_BIN, jarfile))
	xcopy("libs", "_build/jar/libs")

	if exist("_build/data/###export/"):
		xcopy("_build/data/###export/", "_build/jar")
	delete("_build/jar/manifest.mf")

def execute():
	cls()
	if not exist("_build/jar"):
		echoerror("No jar file found. Run debug first")
		Error()
		return

	filelist = ListFile("./_build/jar", "*.jar")
	if len(filelist) == 0:
		echoerror("No jar file found. Run debug first")
		Error()
		return

	run('"%s"java -Xmx1g -jar %s'%(JAVA_BIN, filelist[0]))



def main(argv):
	global JAVA_BIN
	try:
		JAVA_HOME
	except NameError:
		echoerror("JAVA_HOME is not set")
		return

	if not exist(JAVA_HOME):
		echoerror("JAVA_HOME not found [%s]"%(JAVA_HOME))
		return

	JAVA_BIN = JAVA_HOME + "/bin/"
	pushd(GetPath(argv[0]))
	for i in range(1, len(argv)):
		p = argv[i].lower()
		if p == "debug":
			debug()
		elif p == "run":
			execute()
		elif p == "data":
			makedata()
		else:
			echoerror("invalid command")
	popd()

if __name__ == '__main__':
	main(sys.argv)





